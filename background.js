// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

let  currentTab = null;

// Set batch mode to false when starting extension
chrome.storage.sync.set({ batchMode: false }, function () {});
chrome.storage.sync.set({ tabs: {} }, function () {});

function saveURL(url) {
  chrome.storage.sync.get(["batchMode", "urls"], function (data) {
    if (data.batchMode) {
      let urls = [];
      if (data.urls) urls = data.urls;
      urls.push(url);
      chrome.storage.sync.set({ urls: urls }, function () {});
    }
  });
  chrome.storage.sync.set({ lastUrl: url }, function () {});
}

const saveMpd = function (details) {
  var url = details.url;
  var check = url.toLowerCase();
  if (check.includes("https://www.youtube.com")) {
    url = url.replace("&fmt=json3", "&fmt=ttml");
  }
  if (check.includes(".ass") || check.includes(".vtt") || check.includes(".srt") || check.includes(".xml") || check.includes("timedtext") || check.includes("/?o=") || check.includes("/subtitle") || (check.includes("v.vrv.co") & check.includes(".txt"))) {
    if (check.indexOf("log") === -1 && check.indexOf('fly.prd.media.h264.io') === -1 && check.indexOf('adobeorg.xml') === -1 && check.indexOf('metrics') === -1 && check.indexOf('tracking') === -1 && check.indexOf('ContentFilterSubtitleSupported') === -1 && check.indexOf('local.adguard.org') === -1){
      saveURL(url);
      chrome.storage.sync.get(["tabs", "batchMode"], function (data) {
        let tabs = data.tabs;
        const tabId = details.tabId;
        if (data.batchMode) tabs[tabId] = true;
        else tabs = { [tabId]: true };
        chrome.storage.sync.set({ tabs: tabs }, function () {
          updateBadge();
        });
      });
    }
  }
};

chrome.webRequest.onBeforeRequest.addListener(
  saveMpd,
  { urls: ["<all_urls>"] },
  []
);

function updateBadge() {
  chrome.storage.sync.get(["tabs", "batchMode"], function (data) {
    let color = "rgb(66,133,244)";
    if (data.batchMode) {
      if (data.tabs[currentTab]) color = "green";
      else color = "red";
    } else if (data.tabs[currentTab]) {
      chrome.action.setBadgeText({ text: "Found" });
    } else {
      chrome.action.setBadgeText({ text: "" });
    }
    chrome.action.setBadgeBackgroundColor({ color: color });
  });
}

// Listen for reloading pages
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  if (changeInfo.status === "loading") {
    chrome.storage.sync.get("tabs", function (data) {
      let tabs = data.tabs;
      tabs[tabId] = false;
      chrome.storage.sync.set({ tabs: tabs }, function () {
        updateBadge();
      });
    });
  }
});

// Listen for changing tabs
chrome.tabs.onActivated.addListener(function (activeInfo) {
  currentTab = activeInfo.tabId;
  updateBadge();
}); 
