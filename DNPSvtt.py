import requests
from datetime import datetime
from urllib.parse import urljoin
import re

def download_file_content(url):
    response = requests.get(url)
    if response.status_code == 200:
        return response.text
    elif response.status_code == 404:
        print(f"File not found at {url}. Start Muxing.")
        return None
    else:
        print(f"Failed to download file from {url}. Status code: {response.status_code}")
        return None

def filename():
    now = datetime.now()
    formatted_date = now.strftime("%y_%m_%d-%H_%M_%S")
    return f"{formatted_date}.vtt"

def vtt(starting_file_url):
    combined_content = ""
    file_index = 0

    while True:
        file_url = urljoin(starting_file_url, f'seg_{file_index:05d}.vtt')
        file_content = download_file_content(file_url)

        if file_content is None:
            break
        
        file_content_match = re.search(r'WEBVTT(.*?\n\nSTYLE.*?\})', file_content, re.DOTALL)
        
        if file_content_match:
            combined_content += file_content_match.group(0) + '\n\n'
        else:
            print(f"Failed to extract content from {file_url}")
        file_lines = file_content.split('\n')
        filtered_lines = [line for line in file_lines if all(keyword not in line for keyword in ['WEBVTT', 'STYLE', '::cue()', 'font-family', '}', 'text-shadow', 'color'])]
        file_content = '\n'.join(filtered_lines)
        combined_content += file_content + '\n\n'
        file_index += 1

    combined_content = re.sub(r'\n{3,}', '\n\n', combined_content)
    name = filename()

    with open(name, 'w', encoding='utf-8') as output_file:
        output_file.write(combined_content)

    print(f"DNPS vtt combined in {name}")

url = input("Enter any part of the DNPS vtt file URL: ")

vtt(url)
