# Subtitle-Detector
Small extension that will allow you to get the subtitiles files from any page that uses them.

## Tested
| Site | Subtitile Type |
| ---- | -------------- |
| [Netflix](https://www.netflix.com/) | Xml(.xml) |
| [Bilibili](https://www.bilibili.tv/) | Advanced Sub Station Alpha(.ass) |
| [Bilibili](https://www.bilibili.tv/) | Bilibili json(.bcc) |
| [YouTube](https://www.youtube.com/) | Timed Text 1.0(.ttml) |
| [Iqiyi](https://iq.com/) | Xml(.xml) |
| [Vieon](https://vieon.vn/) | WebVTT(.vtt) |
| [Max](https://play.max.com/) | WebVTT(.vtt) |
| [Facebook](https://www.facebook.com/) | SubRip(.srt) |
| [Disney Plus](https://www.disneyplus.com/) | WebVTT(.vtt) |
| [SBS](https://www.sbs.com.au/) | WebVTT(.vtt) |
| [Crunchyroll](https://www.crunchyroll.com/) | Advanced Sub Station Alpha but it store on their server as txt (.txt) |
<br>
I will update if have more site was tested

## Support
Support more unencrypted subtitile file ('.xml', '.ass', '.vtt', 'timedtext', 'bilibilisub')
<br>
If you found more site have unencrypted subtitle and want to tell me to add it, you can DM or ping me on Discord

## Note - Really Big Note:
For HBOMax and Disney Plus:
<br>
You can use my Script I give in same folder with this extension to get full vtt file for HBOMax and Disney Plus.
<br>
<br>
For Crunchyroll:
<br>
You need to change their file from .txt to .ass because some language will give you Broken Subtiltile like Vietnamese

## Credit
Original extension from [wks_uwu](https://github.com/SASUKE-DUCK/MPD-VTT-ASS-Detector)
<br>
Updated to Manifest v3 and add more support file to 1 extension by [annnekkk](https://gitlab.com/Annnekkk)
